<?php
/** Custom */

//$settings['install_profile'] = 'standard';
$settings['hash_salt'] = '3MzTPgRc_bE8eiJFiyuDv41btcdN5-SG3JldTV2ZtfTjocI0Z6nJ8QeqTaS5vqu68-8C-Rac4Q';

$databases['default']['default'] = array (
  'database' => 'drupal10',
  'username' => 'drupal10',
  'password' => 'drupal10',
  'prefix' => '',
  'host' => 'database',
  'port' => '3306',
  'isolation_level' => 'READ COMMITTED',
  'namespace' => 'Drupal\\mysql\\Driver\\Database\\mysql',
  'driver' => 'mysql',
  'autoload' => 'core/modules/mysql/src/Driver/Database/mysql/',
);

$settings['config_sync_directory'] = '../config/sync';

/** Private files */
$settings['file_private_path'] = 'sites/default/private';

/** Trusted hosts */
$settings['trusted_host_patterns'] = [
  '^localhost$',
  '^.+\.localhost$',
  '^.+\.lndo\.site',
];

/** === NO CACHE DEVELOPMENT === */

/** Enable local development services. */
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';

/** Show all error messages, with backtrace information. */
$config['system.logging']['error_level'] = 'verbose';

/** Disable CSS and JS aggregation. */
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

/** Disable the render cache. */
$settings['cache']['bins']['render'] = 'cache.backend.null';
/** Disabel Bootstraop cache */
$settings['cache']['bins']['bootstrap'] = 'cache.backend.null';
/** Disabel Discovery cache */
$settings['cache']['bins']['discovery'] = 'cache.backend.null';
/** Disable caching for migrations. */
$settings['cache']['bins']['discovery_migration'] = 'cache.backend.memory';
/** Disable Internal Page Cache. */
$settings['cache']['bins']['page'] = 'cache.backend.null';
/** Disable Dynamic Page Cache. */
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
/** Disable data cache */
$settings['cache']['bins']['data'] = 'cache.backend.null';
/** Disable config cache */
$settings['cache']['bins']['config'] = 'cache.backend.null';

/** Local dev service. */
if (file_exists($app_root . '/' . $site_path . '/local.services.yml')) {
  $settings['container_yamls'][] = $app_root . '/' . $site_path . '/local.services.yml';
}
