# Drupalea

Proyecto base para la sesion de introducción al desarrollo de módulos en drupal 10

## Taller/workshop: Introducción al desarrollo de módulos en Drupal 10

La idea es ver como comenzar a realizar un desarrollo de módulos en Drupal 10.

En el taller se mostrara la creación de un modulo básico, veremos ejemplos existentes y como poder aprovecharlos para hacer nuestros desarrollos a medida.

Es una sesión actualizada de la que ya di en la DrupalCamp 2014: https://2014.drupalcamp.es/es/node/210.html

Se explicara por encima algunos conceptos.

Puedes facilitar lo que deseas encontrarte en el taller, de esta manera intentare ajustarme a lo que los asistentes desearían conocer :)

Formulario para ajustar las necesidades de las ponencias: https://docs.google.com/forms/d/e/1FAIpQLSdCblnLhdSCxtrG1gbiBJbQ7baZNH3x6yBFlL9lJpn5LCxnwQ/viewform

Sera necesario tener instalado el entorno de desarrollo, un entorno con Drupal en LAMP, MAMP, WAMP o similar para poder trabajar.

### Recomendación

- Drupal 10. Instalado y funcionando.
- IDE de desarrollo. En la maquina virtual estara disponible Code.
- Se recomienda usar la maquina virtual para facilitar la sesión.

Algunos módulos que veremos, por ello es aconsejable instalarlos.

- Modulo Devel
- Modulo Coder
- Módulo Examples

Están disponibles maquinas virtuales o instaladores para facilitar:

- Docker (+ Lando) / Maquina virtual
- http://drupalcamp2023.keopx.net/ (recomendado)

## Notas

### Comandos para crear el proyecto desde 0

```
lando start
lando composer install
lando drush si --db-url=mysql://drupal10:drupal10@database/drupal10 -y
lando drush uli
```

### Comandos para crear el proyecto desde base de datos
```
lando db-import drupalea.sql.gz
lando start
lando composer install
lando drush cr
lando drush uli
lando drush cim -y
```

